import { Component, OnInit } from '@angular/core';
import { NamesService } from '../names.service';
import { Names } from '../model/names'

@Component({
  selector: 'app-names',
  templateUrl: './names.component.html',
  styleUrls: ['./names.component.css']
})
export class NamesComponent implements OnInit {
  names: Names[];
  api:any;

  constructor(private service: NamesService) {
    service.getHelloWorld().subscribe(data => {
      this.names = data;
    })
    service.getHealthApi().subscribe(data => {
      this.api = data;

      console.log(data);
    })
   }

  ngOnInit(): void {  
  }

}
