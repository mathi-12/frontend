import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Names } from './model/names'
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class NamesService {
  
  constructor(private http:HttpClient) { 
        
  }

  getHelloWorld():Observable<any>  {
    return this.http.get('http://localhost:8080/api/v1/names');
  }

  getHealthApi():Observable<any> {
    return this.http.get('http://localhost:8080/actuator/health');
  }
}
